import React, { useState } from "react";
import { ReactComponent as CloseMenu } from "../assets/x.svg";
import { ReactComponent as MenuIcon } from "../assets/menu.svg";
import Logo  from "../assets/acme.png";
import "./header.css";

const Header = () => {
  const [click, setClick] = useState(false);
  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);
  return (
    <div className="header col-md-9">
      <div className="logo-nav">
        <div className="logo-container">
          <a href="#">
            <img src={Logo} className="logo" />
          </a>
          <div className="logo-text">
            <div>
              <span className="rp">Research Profissional</span>
            </div>
            <div>
              <span className="platform">Platform</span>
            </div>
          </div>
        </div>
        <div className="vl"></div>
        <div className="nav-options-wrapper">
          <ul className={click ? "nav-options active" : "nav-options"}>
            <li className="option" onClick={closeMobileMenu}>
              <a href="#">Home</a>
            </li>
            <li className="option" onClick={closeMobileMenu}>
              <a href="#">About Us</a>
            </li>
            <li className="option" onClick={closeMobileMenu}>
              <a href="#">Insights</a>
            </li>
            <li className="option" onClick={closeMobileMenu}>
              <a href="#">Events</a>
            </li>
            <li className="option" onClick={closeMobileMenu}>
              <a href="#">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
      
      <div className="mobile-menu" onClick={handleClick}>
        {click ? (
          <CloseMenu className="menu-icon" />
        ) : (
          <MenuIcon className="menu-icon" />
        )}
      </div>
    </div>
  );
};

export default Header;
