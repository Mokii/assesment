import React from "react";
import Footer from "./footer/Footer";
import Header from "./header/Header";
import Home from "./pages/Home";
import "./styles.css";


export default function App() {
  return (
    <div className="App">
      <Header />
      <Home />
      <Footer />
    </div>
  );
}
