import React from 'react'
import './footer.css'
const twitterImg = require('./../assets/twitter.png')
const fbImg = require('./../assets/fb.png')
const inImg = require('./../assets/in.png')
const emailImg = require('./../assets/email.png')

const Footer = () => {
    return (
        <div className="footer-wrapper col-md-12">
            <div className="footer col-md-9">
                <div className="row">
                    <div className="info-text col-md-6">
                        <span>
                            Call us at 111-222-333
                        </span><br />
                        <span>
                            for more information
                        </span>
                    </div>
                    <div className="col-md-6">
                        <div className="social-share">
                            Social Share 
                            <img src={twitterImg}/>
                            <img src={fbImg}/>
                            <img src={inImg}/>
                            <img src={emailImg}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer
