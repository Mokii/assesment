import React from 'react'
import AcmeInsight from './contents/AcmeInsight'
import AcmeWealth from './contents/AcmeWealth'
import ContactUs from './contents/ContactUs'
import Upcoming from './contents/Upcoming'
import './pages.css'


const Home = () => {
    return (
        <div className="main-content">
            <AcmeWealth />
            <AcmeInsight />
            <ContactUs />
            <Upcoming />
        </div>
    )
}

export default Home
