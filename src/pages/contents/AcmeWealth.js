import React from 'react'
const acmeWealthImg = require('./../../assets/acme-wealth.png')

const AcmeWealth = () => {
    return (
        <>
            <div className="acme-wealth"
                style={{
                    backgroundImage: `url(${acmeWealthImg})`,
                    backgroundSize: 'cover',
                }}
            >
                {/* <img src={acmeWealthImg} /> */}
                <div class="acme-wealth-centered col-md-9">
                    <div className="acmew-text-wrapper col-md-6">

                        <p class="acme-wealth-text">ACME Wealth Management Platforms</p>
                        <p className="awmp">
                            Investment excellence.<br/>
                            Diversity of thought.<br/>
                            Organizational strength.
                        </p>
                    </div>
                </div>
            </div> 
        </>
    )
}

export default AcmeWealth
