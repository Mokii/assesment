import React from 'react'
const eventsImg = require('./../../assets/events-bg.png')


const Upcoming = () => {
    return (
        <>
            <div className='events-insight-wrapper col-md-9'>
                <div className="event-intro">
                    <div className='events-insight-title'>
                        <h3>Upcoming Events</h3>
                    </div>
                    <div className='events-insight-text'>How are the factors being used around the world?</div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="events-insight-box box-1" 
                            style={{
                                backgroundImage: `url(${eventsImg})`,
                                backgroundSize: 'cover'
                            }}
                        >
                            <div className="events-date">
                                <div className="event-month">Jan</div>
                                <div className="event-day">28</div>
                            </div>
                            <div className="event-desc">
                                <div className="desc-title">Insight Exchange Network</div>
                                <div className="desc-text">Join us for this showcasing innovation.</div>
                            </div>
                            <div className="event-btn">
                                <button>
                                    Get More Insight
                                </button>
                            </div>
                            <div className="event-location">
                                Chicago , <span>IL</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="events-insight-box box-2" 
                            style={{
                                backgroundImage: `url(${eventsImg})`,
                                backgroundSize: 'cover'
                            }}
                        >
                            <div className="events-date">
                                <div className="event-month">Feb</div>
                                <div className="event-day">12</div>
                            </div>
                            <div className="event-desc">
                                <div className="desc-title">Citywide Buyer's Retreat</div>
                                <div className="desc-text">Find oout how are responding to the changing future of interest...</div>
                            </div>
                            <div className="event-btn">
                                <button>
                                    Get More Insight
                                </button>
                            </div>
                            <div className="event-location">
                                The Wagner, <span>New York</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="events-insight-box box-3" 
                            style={{
                                backgroundImage: `url(${eventsImg})`,
                                backgroundSize: 'cover'
                            }}
                        >
                            <div className="events-date">
                                <div className="event-month">May</div>
                                <div className="event-day">6</div>
                            </div>
                            <div className="event-desc">
                                <div className="desc-title">Research Exchange</div>
                                <div className="desc-text">Find the best online resource to help with your investments...</div>
                            </div>
                            <div className="event-btn">
                                <button>
                                    Get More Insight
                                </button>
                            </div>
                            <div className="event-location">
                                London, <span>England</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Upcoming
