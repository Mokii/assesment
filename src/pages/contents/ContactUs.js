import React from 'react'
const acmeContactUsImg = require('./../../assets/contact-us-bg.png')

const ContactUs = () => {
    return (
        <>
            <div className="contact-us" 
                style={{
                    backgroundImage: `url(${acmeContactUsImg})`,
                    backgroundSize: 'cover'
                }}
            >
                {/* <img src={acmeContactUsImg} /> */}
                <div class="contact-us-centered col-md-9">
                    <p class="contact-us-text1 col-md-10">Our Commitment to Professionals</p>
                    <div class="contact-us-text2 col-md-12">
                        We help our partners deliver industry leading results with a commitment 
                        to excellence, thought-provoking insights and experience distribution. 
                        We are later focused on our share goals - helping clients achieve their 
                        objectives.
                    </div>
                    <div className="col-md-10 text-center contact-us-btn">
                        <button>Contact Us</button>
                    </div>
                </div>
            </div> 
        </>
    )
}

export default ContactUs
