import React from 'react'

const AcmeInsight = () => {
    return (
        <>
            <div className='acme-insight-wrapper col-md-9'>

                <div className='acme-insight-title'>
                    <h3>ACME Insights</h3>
                </div>
                <div className='acme-insight-text'>How are the factors being used around the world?</div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="acme-insight-box box-1">
                            <img src={require('./../../assets/acme-insight-1.png')} />
                            <div className="acme-insight-box-text1">
                                Global Factor <br/>Investing Study
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="acme-insight-box box-2">
                            <img src={require('./../../assets/acme-insight-2.png')} />
                            <div className="acme-insight-box-text2">
                                2019 <br/>Outlook
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="acme-insight-box box-3">
                            <img src={require('./../../assets/acme-insight-3.png')} />
                            <div className="acme-insight-box-text3">
                                Capital Market <br/>Assumptions
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AcmeInsight
